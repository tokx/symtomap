"""symptomap URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
from map.models import Symptom, Disease, SickTime


class SymptomSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Symptom
        fields = ['name', 'description']



# ViewSets define the view behavior.
class SymptomViewSet(viewsets.ModelViewSet):
    queryset = Symptom.objects.all()
    serializer_class = SymptomSerializer


class DiseaseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Disease
        fields = ['name', 'description']


# ViewSets define the view behavior.
class DiseaseViewSet(viewsets.ModelViewSet):
    queryset = Disease.objects.all()
    serializer_class = DiseaseSerializer

class SickTimeSerializer(serializers.HyperlinkedModelSerializer):
    symptoms = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='symptom-detail')
    diseases = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='disease-detail')

    class Meta:
        model = SickTime
        fields = ['symptoms', 'diseases','severity','location','longitude','latitude','start_date','end_date']

# ViewSets define the view behavior.
class SickTimeViewSet(viewsets.ModelViewSet):
    queryset = SickTime.objects.all()
    serializer_class = SickTimeSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'symptoms', SymptomViewSet)
router.register(r'sicktime', SickTimeViewSet)
router.register(r'disease', DiseaseViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('map/', include('map.urls')),
    path(r'api-auth/', include('rest_framework.urls')),
    path(r'api/', include(router.urls)),
    path('', include('map.urls')),
]
