from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
import json
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.express as px
from datetime import datetime as dt
from django.utils import timezone as tz
from geopy.geocoders import Nominatim
import plotly.graph_objects as go


from dash.dependencies import Input, Output, State

from django_plotly_dash import DjangoDash

from map.models import Symptom, Disease
from map.models import SickTime

app = DjangoDash('DashMap')
geolocator = Nominatim(user_agent="DashMap")
mapbox_access_token = settings.MAPBOX_ACCESS_TOKEN




@app.callback(Output('main_graph', 'figure'),
              [
               Input('map_date_picker', 'start_date'),Input('map_date_picker', 'end_date'),dash.dependencies.Input('output-container-button', 'children')],
              [State('main_graph', 'relayoutData')])
def make_main_figure(start_date,end_date,children_container_button,main_graph_layout):
    sicktimes_data = SickTime.objects.prefetch_related('symptoms', 'diseases').filter(start_date__gte=start_date,end_date__lte=end_date)

    symptoms_data = pd.DataFrame(list([sicktime.symptoms.values() for sicktime in sicktimes_data]))
    diseases_data = pd.DataFrame(list([sicktime.diseases.values() for sicktime in sicktimes_data]))

    sick_time_data = pd.DataFrame(list(SickTime.objects.select_related('symptoms').select_related('diseases').filter(start_date__gte=start_date,end_date__lte=end_date).values()))
    #sick_time_data =  pd.DataFrame(list(data))

    fig = go.Figure(go.Scattermapbox(
        lat=sick_time_data['latitude'],
        lon=sick_time_data['longitude'],
        mode="markers",
        marker=dict(
            size=9,
            color=sick_time_data['severity'],
            showscale=True,
            colorscale=[[0, 'green'],
                        [1, 'red']],
            cmin=0,
            cmax=1
        ),
        text=sick_time_data['severity'],
    ))
    fig.update_layout(
        autosize=True,
        hovermode='closest',
        mapbox=dict(
            accesstoken=mapbox_access_token,
            bearing=0,
            center=dict(
                lat=51,
                lon=12
            ),
            pitch=0,
            zoom=4
        ),
    )

    return fig


@app.callback(
    dash.dependencies.Output('output-container-button', 'children'),
    [dash.dependencies.Input('submit-button', 'n_clicks')],
    [dash.dependencies.State('symptoms', 'value'),
     dash.dependencies.State('diseases', 'value'),
     dash.dependencies.State('illness-datepicker', 'start_date'),
     dash.dependencies.State('illness-datepicker', 'end_date'),
     dash.dependencies.State('severity-slider','value'),
     dash.dependencies.State('addressfield', 'value')])
def update_output(n_clicks, symptoms, diseases, start_date, end_date,severity,address):
    error = ""
    if (address == None):
        return "Please set a valid address"

    location = geolocator.geocode(address)
    if (location == None):
        return "Your address could not be found. Please correct your entry."

    if start_date == None:
        return "Please set at least a valid start date"

    if symptoms == None:
        return "Please add a symptom that matches your state or give us feedback so that we can add that symptom"

    selected_symptoms = Symptom.objects.filter(id__in=symptoms)

    sick_time = SickTime.objects.create(severity=severity,start_date=start_date,end_date=end_date,location=str(location.latitude)+","+str(location.longitude),longitude=location.longitude,latitude=location.latitude)
    sick_time.symptoms.set(selected_symptoms)

    if diseases != None:
        selected_diseases = Disease.objects.filter(id__in=diseases)
        sick_time.diseases.set(selected_diseases)

    return "Thank you we have updated our map with your symptom"




def index(request):
    app.layout = html.Div(
        children=[
        html.Div(
            className='row',
            children=[
            html.H1(
                'Symtomap'
            ),
        ]),
        html.Div(
            className='row',
            children=[
            html.Div(
                id='options_container',
                className='pretty_container six columns',
                children=[
                    html.H3(
                        'Add your symptoms now and anonymous to help and find contagion centers'
                    ),
                    html.P(
                        'Please add your Symptoms:',
                        className="control_label"
                    ),
                    dcc.Dropdown(
                        id='symptoms',
                        options=[{'label': str(symptom[1]),
                        'value': symptom[0]}
                       for symptom in Symptom.objects.values_list('id', 'name')],
                        multi=True,
                        className="dcc_control"
                    ),
                    html.P(
                        'Select your Disease (only if diagnosed)',
                        className="control_label"
                    ),
                    dcc.Dropdown(
                        id='diseases',
                        options=[{'label': str(disease[1]),
                                  'value': disease[0]}
                                 for disease in Disease.objects.values_list('id', 'name')],
                        multi=True,
                        className="dcc_control"
                    ),
                    html.P(
                        'Time of Illness (Leave end date empty if your desease just started):',
                        className="control_label"
                    ),
                    html.Div([
                        dcc.DatePickerRange(
                            id='illness-datepicker',
                            min_date_allowed=tz.datetime(2019, 1, 1),
                            max_date_allowed=tz.now(),
                            initial_visible_month=tz.now(),
                        ),
                        html.Div(id='output-container-date-picker-range')
                    ]),
                    html.P(
                        'Severity:',
                        className="control_label"
                    ),
                    dcc.Slider(
                        id='severity-slider',
                        min=0,
                        max=1,
                        step=0.1,
                        value=0.5,
                    ),
                    html.P(
                        'Address:',
                        className="control_label"
                    ),
                    dcc.Input(id="addressfield", type="text", debounce=True,
                              placeholder="e.g. Peterstraße 3, 28739 Yourcity, Yourcountry"),
                    html.Div([
                        html.Button('Submit', id='submit-button'),
                        html.Div(id='output-container-button',
                                 children='')
                    ])
                ]),
            html.Div(
                className='pretty_container six columns',
                id='main_graph_container',
                children=[
                    html.H3(
                        'Analyze your sourroundings'
                    ),
                    dcc.Graph(id='main_graph'),
                    html.P(
                        'Select a time interval:',
                        className="control_label"
                    ),
                    html.Div([
                        dcc.DatePickerRange(
                            id='map_date_picker',
                            min_date_allowed=tz.datetime(2019, 1, 1),
                            max_date_allowed=tz.now(),
                            start_date=tz.datetime(2019, 1, 1),
                            end_date=tz.now(),
                            initial_visible_month=tz.now(),
                        )
                    ]),
                ],
            ),
        ]),
    ])



    return render(request, 'index.html')
