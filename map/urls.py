from django.urls import path, include

from map.models import Symptom
from . import views



urlpatterns = [
    path(r'', views.index, name='index'),
    path(r'', include('django_plotly_dash.urls')),
]