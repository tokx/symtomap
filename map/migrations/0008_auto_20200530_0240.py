# Generated by Django 3.0.4 on 2020-05-30 00:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0007_sicktime_diseases'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Desease',
            new_name='Disease',
        ),
    ]
