# Generated by Django 3.0.4 on 2020-03-22 22:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0004_auto_20200322_2225'),
    ]

    operations = [
        migrations.AddField(
            model_name='sicktime',
            name='latitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sicktime',
            name='longitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
    ]
