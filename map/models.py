from django.db import models

# Create your models here.
from django.db import models
from translations.models import Translatable
from location_field.models.plain import PlainLocationField


class Symptom(Translatable):
    name = models.CharField(max_length=200)
    description = models.TextField()
    class TranslatableMeta:
        fields = ['name', 'description']


class Disease(Translatable):
    symptoms = models.ManyToManyField(Symptom)
    name = models.CharField(max_length=200)
    description = models.TextField()
    class TranslatableMeta:
        fields = ['name', 'description']

class SickTime(models.Model):
    symptoms = models.ManyToManyField(Symptom)
    diseases = models.ManyToManyField(Disease)
    severity = models.FloatField(max_length=2)
    location = PlainLocationField(based_fields=['city'],default='-22.2876834,-49.1607606')
    longitude =  models.FloatField()
    latitude =  models.FloatField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null=True)
