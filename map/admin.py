from django.contrib import admin

# Register your models here.
from translations.admin import TranslatableAdmin, TranslationInline

from map.models import Symptom, Disease, SickTime


class ContinentAdmin(TranslatableAdmin):
    inlines = [TranslationInline,]

admin.site.register(Symptom)
admin.site.register(Disease)
admin.site.register(SickTime)