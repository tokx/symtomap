# symtomap

This project is based on django and is a basic symptom map indicator for possible corova virus infections. It might be suitable to choose areas which are important in case of a quarantaine.
Users can add their symptoms anonymously.

# Development

## First Run Requirements
Please to the following 6 steps to initially start the project on your machine
1. install and run docker
2. install python 3
3. install requiremtents for python

        pip install -r requirements.txt
    
4. Set the following Environment Variables 
    
        SECRET_KEY
    
        MAPBOX_ACCESS_TOKEN
    
        DB_HOST [localhost]
    
        DB_PORT [5432]
    
5. Start the Database and symptomap server

        docker-compose up db
    
6. Run the development server

        python manage.py runserver 8080
    
## Deploying DB changes

When you change models please run the following commands to reflect those changes in the database
        
        python manage.py makemigrations
        python manage.py migrate

## Run with kubernetes locally

1. [Install Minikube](https://kubernetes.io/de/docs/tasks/tools/install-minikube/)
2. Run minikube
        
        minikube start
        minikube dashboard
        
3. Open cmd and run the following commands in this directory to deploy the app to kubernetes locally

        kubectl apply -f deploy/kubernetes/django
        kubectl apply -f deploy/kubernetes/postgres
        
4. To access your deployment from the outside check

        minikube addons enable ingress
        minikube service django-service --url
    
## Translations
If you need to change translations run this command

    python manage.py synctranslations